package com.greatlearning.UserMicrosServices.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.greatlearning.UserMicrosServices.entity.Audit;

@Repository
public interface AuditRepository extends JpaRepository<Audit, Long> {

	// Audit Repository
	
	// query to get the sales per month for last year
	@Query(nativeQuery = true, value = "SELECT * FROM ALL_ORDERS  ")
	List<Audit> getAllOrders();
	
	
}
