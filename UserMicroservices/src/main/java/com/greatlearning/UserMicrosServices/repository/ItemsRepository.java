package com.greatlearning.UserMicrosServices.repository;

import org.springframework.stereotype.Repository;

import com.greatlearning.UserMicrosServices.entity.Items;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

@Repository
public interface ItemsRepository extends JpaRepository<Items, Long> {
// Items Repository
}