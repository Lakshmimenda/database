package com.greatlearning.UserMicrosServices.controller;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.UserMicrosServices.entity.Audit;
import com.greatlearning.UserMicrosServices.service.ItemsServices;

@RestController
@RequestMapping("/Restaurants/ItemsView")
public class UserViewController {

	@Autowired
	private ItemsServices itemsServices;

	// gets the all the orders of the user
	@GetMapping("/allOrders")
	public List<Audit> allOrders(String userName) {
		return itemsServices.allOrders(userName);
	}

	// get orders by date
	@GetMapping("/getOrdersByDate")
	public List<Audit> getOrdersByDate(String userName, String date) throws ParseException {

		return itemsServices.getOrdersByDate(userName, date);

	}

	// get order by price
	@GetMapping("/getOrdersByPrice")
	public List<Audit> getOrdersByPrice(String userName, int price) {

		return itemsServices.getOrdersByPrice(userName, price);

	}

}
