package com.greatlearning.UserMicrosServices.controller;

import java.math.BigInteger;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.UserMicrosServices.entity.Items;
import com.greatlearning.UserMicrosServices.service.ItemsServices;

@RestController
@RequestMapping("/Restaurants/Items")
public class ItemsController {

	// Items Controller that allows user to view, select the items and get the total
	// bill
	@Autowired
	private ItemsServices itemsServices;

	// gets the item by id
	@GetMapping("/checkItem")
	public String checkItemExist(Long id) {
		return itemsServices.checkItemExists(id);
	}

	// gets all the items details
	@GetMapping("/viewItems")
	public List<Items> viewItems() {

		return itemsServices.viewItems();
	}

	// allows the user to select the items based on id
	@PostMapping("/selectItems")
	public List<Items> selectedItems(@RequestBody Collection<Long> ids, String userName) {
		return itemsServices.selectedItems(ids, userName);
	}

	// gets the total bill
	@GetMapping("/totalBill")
	public String totalBill(String userName, String code) {
		return itemsServices.totalBill(userName, code);
	}
}
