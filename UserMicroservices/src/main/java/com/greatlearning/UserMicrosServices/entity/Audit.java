package com.greatlearning.UserMicrosServices.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Audit {

	// Audit Entity

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private String userName;

	@Column
	private String itemName;

	@Column
	private int itemPrice;

	@Column(columnDefinition = "date default sysdate")
	private Date createDate;

	@Column
	private String code;
	
	@Column
	private String city;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(int itemPrice) {
		this.itemPrice = itemPrice;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Audit(String userName, String itemName, int itemPrice, Date createDate) {
		super();
		this.userName = userName;
		this.itemName = itemName;
		this.itemPrice = itemPrice;
		this.createDate = createDate;
		this.city = "Bangalore";
	}

	

	@Override
	public String toString() {
		return "Audit [id=" + id + ", userName=" + userName + ", itemName=" + itemName + ", itemPrice=" + itemPrice
				+ ", createDate=" + createDate + ", code=" + code + ", city=" + city + "]";
	}

	public Audit() {
	}

}
