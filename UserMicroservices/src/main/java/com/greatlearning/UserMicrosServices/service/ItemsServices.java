package com.greatlearning.UserMicrosServices.service;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.Collection;
import java.util.List;

import com.greatlearning.UserMicrosServices.entity.Audit;
import com.greatlearning.UserMicrosServices.entity.Items;

public interface ItemsServices {

	// Items Services that allows the user to preform various users action

	// gets all the items
	public List<Items> viewItems();

	// gets the item by id
	public String checkItemExists(Long id);

	// selects the items by id
	public List<Items> selectedItems(Collection<Long> ids, String userName);

	// gets the total bill
	public String totalBill(String userName, String code);
	
	// gets all the order details for the current user
	public List<Audit> allOrders(String userName);

	// gets the orders for the current user by date
	public List<Audit> getOrdersByDate(String userName, String date) throws ParseException;

	// gets the orders for the current user by price
	public List<Audit> getOrdersByPrice(String userName, int price);
}
