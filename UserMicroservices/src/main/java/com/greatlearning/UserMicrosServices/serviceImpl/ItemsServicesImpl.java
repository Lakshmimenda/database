package com.greatlearning.UserMicrosServices.serviceImpl;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Service;

import com.greatlearning.UserMicrosServices.entity.Items;
import com.greatlearning.UserMicrosServices.repository.AuditRepository;
import com.greatlearning.UserMicrosServices.repository.ItemsRepository;
import com.greatlearning.UserMicrosServices.service.ItemsServices;
import com.greatlearning.UserMicrosServices.entity.Audit;

@Service
public class ItemsServicesImpl implements ItemsServices {

// User Service Impl 
	@Autowired
	private ItemsRepository itemsRepository;

	@Autowired
	private AuditRepository auditRepository;

	@Autowired
	private EntityManager entityManager;

	// gets all the items
	@Override
	public List<Items> viewItems() {
		return itemsRepository.findAll();
	}

	// gets the item by id
	@Override
	public String checkItemExists(Long id) {
		if (!itemsRepository.findById(id).isPresent()) {
			return "Item Does Not Exists";

		} else {
			return "Item Details : " + itemsRepository.getById(id);
		}
	}

	// selects the items by id and enters it in audit table
	@Override
	public List<Items> selectedItems(Collection<Long> ids, String userName) {
		List<Items> selectedItems = itemsRepository.findAllById(ids);
		selectedItems.forEach(item -> {
			auditRepository.saveAndFlush(new Audit(userName, item.getName(), item.getPrice(), new Date()));
		});

		return selectedItems;
	}

	// gets the total bill for the current user
	@Override
	public String totalBill(String userName, String code) {

		String queryStr = "select sum(item_price) from audit where user_name = ?1";
		try {
			Query query = entityManager.createNativeQuery(queryStr);
			query.setParameter(1, userName);

			BigInteger bill = (BigInteger) query.getSingleResult();
			StringBuffer sb = new StringBuffer();
			if (code != null) {
				if (isCouponApplies(userName, code)) {
					sb.append("\nThe Total Bill is " + (Integer.parseInt(bill.toString()) * 50) / 100
							+ "\nCoupon Successfully Applied");
				} else {
					sb.append("\nInvalid Coupon Code");
					sb.append("\nThe Total Bill is " + bill);
				}
			} else {
				sb.append("\nThe Total Bill is " + bill);
			}
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		}

	}

	// checks if the coupon is valid or not
	public boolean isCouponApplies(String userName, String code) {

		String queryStr = "select code from audit where user_name = ?1 and code is not null";
		try {
			Query query = entityManager.createNativeQuery(queryStr);
			query.setParameter(1, userName);

			List result = query.getResultList();
			return result.contains(code);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		}
	}

	// gets all orders for the current user
	@Override
	public List<Audit> allOrders(String userName) {
		List<Audit> audit = auditRepository.getAllOrders();
		return audit.stream().filter(a -> userName.equals(a.getUserName())).collect(Collectors.toList());
	}

	// gets orders by date filter provided by user
	@Override
	public List<Audit> getOrdersByDate(String userName, String date) throws ParseException {
		List<Audit> audit = auditRepository.getAllOrders();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

		Date enteredDate = dateFormat.parse(date);

		return audit.stream().filter(a -> userName.equals(a.getUserName()) && enteredDate.equals(a.getCreateDate()))
				.collect(Collectors.toList());
	}

	// gets the orders by price filter provided by user
	@Override
	public List<Audit> getOrdersByPrice(String userName, int price) {
		List<Audit> audit = auditRepository.getAllOrders();
		return audit.stream().filter(a -> userName.equals(a.getUserName()) && price == a.getItemPrice())
				.collect(Collectors.toList());
	}
}
