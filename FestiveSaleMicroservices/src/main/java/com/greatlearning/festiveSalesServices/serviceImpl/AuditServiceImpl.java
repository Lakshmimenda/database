package com.greatlearning.festiveSalesServices.serviceImpl;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.festiveSalesServices.repository.AuditRepository;
import com.greatlearning.festiveSalesServices.service.AuditService;

@Service
public class AuditServiceImpl implements AuditService {

	// Audit Service Impl

	@Autowired
	private AuditRepository auditRepository;

	@Autowired
	private EntityManager entityManager;

	// gets the coupon code
	@Override
	public String getCouponCode(String userName) {
		auditRepository.updateCoupon(userName);
		return "Special50";
	}

}
