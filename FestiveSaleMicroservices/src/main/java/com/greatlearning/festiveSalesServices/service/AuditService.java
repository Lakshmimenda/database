package com.greatlearning.festiveSalesServices.service;

public interface AuditService {
// Audit Service to get the coupon code
	public String getCouponCode(String userName);
}
