package com.greatlearning.festiveSalesServices;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.sql.SQLException;

import org.h2.tools.Server;



@SpringBootApplication
public class FestiveSalesMicroservices {

	public static void main(String[] args) {
		SpringApplication.run(FestiveSalesMicroservices.class, args);
	}
}