package com.greatlearning.festiveSalesServices.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.festiveSalesServices.service.AuditService;

@RestController
@RequestMapping("/Restaurants/FestiveSales")
public class FestiveSalesController {

	// Festive Sales Controller
	@Autowired
	private AuditService auditService;

	// gets the coupon code
	@GetMapping("/couponCode")
	public String getCouponCode(String userName) {

		return auditService.getCouponCode(userName);
	}

}
