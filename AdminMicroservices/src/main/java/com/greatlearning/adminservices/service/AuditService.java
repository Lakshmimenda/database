package com.greatlearning.adminservices.service;

import java.util.List;
import com.greatlearning.adminservices.entity.Audit;

public interface AuditService {

	// Audit Service Interface
	public List<Audit> getBillForToday();

	public List<Audit> getSalesForTheMonth();
	
	public String getSalesPerCity();

	public String getMaxSalesOfTheMonth();

	public String getSalesPerMonthForLastYear();
	
}
