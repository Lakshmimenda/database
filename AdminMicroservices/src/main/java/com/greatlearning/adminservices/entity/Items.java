package com.greatlearning.adminservices.entity;

import javax.persistence.*;

@Entity
public class Items {

	// Items Entity Class
	@Id
	@Column
	private Long id;

	@Column
	private String name;

	@Column
	private int price;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Items [id=" + id + ", name=" + name + ", price=" + price + "]";
	}

	public Items(Long id, String name, int price) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
	}

	public Items() {
	}

}
