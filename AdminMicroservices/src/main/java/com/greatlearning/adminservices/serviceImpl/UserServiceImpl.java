package com.greatlearning.adminservices.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.adminservices.entity.User;
import com.greatlearning.adminservices.repository.UserRepository;
import com.greatlearning.adminservices.service.UserServices;

@Service
public class UserServiceImpl implements UserServices {

// User Service Impl that performs CRUD on Users	
	@Autowired
	private UserRepository userRepository;

	// gets the users details
	@Override
	public List<User> getUsers() {
		return userRepository.findAll();
	}

	// gets the user by id
	@Override
	public String getUsersbyId(Long id) {
		if (!userRepository.findById(id).isPresent()) {
			return "User Does Not Exists";

		} else {
			return "User Details : " + userRepository.getById(id);
		}
	}

	// updates the user
	@Override
	public String updateUser(Long id, User user) {
		if (!userRepository.findById(id).isPresent()) {
			return "User Does Not Exists";
		} else {
			userRepository.deleteById(id);
			userRepository.saveAndFlush(user);
			return "User Updated : " + userRepository.getById(user.getId());
		}
	}

	// registers the user
	@Override
	public String registerUser(User user) {
		if (!userRepository.findById(user.getId()).isPresent()) {

			userRepository.save(user);
			return "User Registered : " + userRepository.getById(user.getId());

		} else {
			return "User Already Exists";
		}
	}

	// deletes the user
	@Override
	public String deleteUser(Long id) {
		if (!userRepository.findById(id).isPresent()) {
			return "User Does Not Exists";
		} else {
			userRepository.deleteById(id);
			return "User Deleted";
		}
	}

}
