package com.greatlearning.adminservices.serviceImpl;

import java.math.BigInteger;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.adminservices.entity.Audit;
import com.greatlearning.adminservices.repository.AuditRepository;
import com.greatlearning.adminservices.service.AuditService;

@Service
public class AuditServiceImpl implements AuditService {

	// Audit Service Impl to get the sales details
	@Autowired
	private AuditRepository auditRepository;

	// gets the bill for the day
	@Override
	public List<Audit> getBillForToday() {
		return auditRepository.getAuditForToday();
	}

	// gets the sales for the month
	@Override
	public List<Audit> getSalesForTheMonth() {
		return auditRepository.getAuditForCurrentMonth();
	}

	// gets sales details per city
	@Override
	public String getSalesPerCity() {

		List<Object[]> audit = auditRepository.getSalesPerCity();
		JSONObject sales = new JSONObject();

		for (Object[] a : audit) {
			sales.put("Sales for the City " + a[0], a[1]);
		}

		return sales.toString();
	}

	// gets max sales of the month
	@Override
	public String getMaxSalesOfTheMonth() {
		List<Object[]> audit = auditRepository.getMaxSalesOfTheMonth();
		JSONObject sales = new JSONObject();

		for (Object[] a : audit) {
			JSONObject result = new JSONObject();
			result.put("Month", a[0]);
			result.put("User Name", a[1]);
			result.put("Price", a[2]);
			sales.put("Sales", result);
		}

		return sales.toString();
	}

	// gets sales per month for the last year
	@Override
	public String getSalesPerMonthForLastYear() {

		List<Object[]> audit = auditRepository.getSalesPerMonthForLastYear();
		JSONObject sales = new JSONObject();
		audit.forEach(a -> {

			sales.put("Sales for Month " + a[0], a[1]);
		});
		return sales.toString();
	}

}
