package com.greatlearning.adminservices.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.greatlearning.adminservices.entity.Audit;

@Repository
public interface AuditRepository extends JpaRepository<Audit, Long> {

	// Audit Repository

	// query to get the sales for the day
	@Query("select a from Audit a where a.createDate = CURRENT_DATE")
	List<Audit> getAuditForToday();

	// query to get the sales for the month
	@Query("select a from Audit a where  MONTH(createDate) = MONTH(CURRENT_DATE)")
	List<Audit> getAuditForCurrentMonth();

	// query to get the sales for the different cities
	@Query(nativeQuery = true, value = "SELECT * FROM SALES_PER_CITY ")
	List<Object[]> getSalesPerCity();

	// query to get the max sales for the month
	@Query(nativeQuery = true, value = "SELECT * FROM MAX_SALES_OF_THE_MONTH ")
	List<Object[]> getMaxSalesOfTheMonth();

	// query to get the sales per month for last year
	@Query(nativeQuery = true, value = "SELECT * FROM MONTHLY_SALES_OF_LAST_YEAR ")
	List<Object[]> getSalesPerMonthForLastYear();

}
