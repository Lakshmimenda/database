package com.greatlearning.adminservices.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.adminservices.service.AuditService;

@RestController
@RequestMapping("/Restaurants/AdminView")
public class AdminViewController {

	// Admin view controller to retrieve the sales details from Audit table for
	// different view
	@Autowired
	private AuditService auditService;

	// gets the sales for the per city
	@GetMapping("/getSalesPerCity")
	public String getSalesPerCity() {

		return auditService.getSalesPerCity();
	}

	// gets the max sales for the month
	@GetMapping("/getMaxSalesOfTheMonth")
	public String getMaxSalesOfTheMonth() {

		return auditService.getMaxSalesOfTheMonth();
	}

	// gets the sales per month for the last year
	@GetMapping("/getSalesPerMonthForLastYear")
	public String getSalesPerMonthForLastYear() {

		return auditService.getSalesPerMonthForLastYear();
	}
}
