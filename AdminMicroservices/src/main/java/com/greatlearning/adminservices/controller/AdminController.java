package com.greatlearning.adminservices.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.adminservices.entity.Audit;
import com.greatlearning.adminservices.entity.User;
import com.greatlearning.adminservices.service.UserServices;

@RestController
@RequestMapping("/Restaurants/Admin")
public class AdminController {

	// Admin Controller performing all the CRUD operations on the Users
	@Autowired
	private UserServices userServices;

	// gets all the user details
	@GetMapping("/getAllUsers")
	public List<User> getAllUsers() {
		return userServices.getUsers();
	}

	// gets the user details by id
	@GetMapping("/getUserById")
	public String getUserById(Long id) {
		return userServices.getUsersbyId(id);
	}

	// registers the user
	@PostMapping("/registerUser")
	public String registerUser(@Validated @RequestBody User user) {
		return userServices.registerUser(user);
	}

	// updates the user
	@PutMapping("/updateUser")
	public String updateUser(Long id, @RequestBody User user) {
		return userServices.updateUser(id, user);
	}

	// deletes the user
	@DeleteMapping("/deleteUser")
	public String deleteUser(Long id) {
		return userServices.deleteUser(id);
	}

}