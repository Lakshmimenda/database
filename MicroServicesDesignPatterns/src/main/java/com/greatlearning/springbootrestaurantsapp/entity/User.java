package com.greatlearning.springbootrestaurantsapp.entity;

import javax.persistence.*;

@Entity
public class User {

	//User Entity Class
	@Id
	@Column
	private Long id;

	@Column(nullable = false)
	private String userName;

	@Column
	private String password;

	@Column
	private boolean enabled;

	@Column
	private String role;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public User(Long id, String userName, String password, boolean enabled, String role) {
		super();
		this.id = id;
		this.userName = userName;
		this.password = password;
		this.enabled = enabled;
		this.role = role;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", userName=" + userName + ", password=" + password + ", enabled=" + enabled
				+ ", role=" + role + "]";
	}

	
	public User() {
	}
	 
}