package com.greatlearning.springbootrestaurantsapp.controller;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.springbootrestaurantsapp.entity.Audit;
import com.greatlearning.springbootrestaurantsapp.feignClients.UserViewClient;

@RestController
@RequestMapping("/Restaurants/UserView")
public class UserViewController {

	@Autowired
	private UserViewClient userViewClient;

	// gets the all the orders of the user
	@GetMapping("/allOrders")
	public List<Audit> allOrders() {
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		return userViewClient.allOrders(userName);
	}

	// get orders by date
//	"Enter the date in mm/dd/yyyy format"
	@GetMapping("/getOrdersByDate")
	public List<Audit> getOrdersByDate(String date) throws ParseException {
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();

		return userViewClient.getOrdersByDate(userName, date);

	}

	// get order by price
	@GetMapping("/getOrdersByPrice")
	public List<Audit> getOrdersByPrice(int price) throws ParseException {
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		return userViewClient.getOrdersByPrice(userName, price);

	}

}
