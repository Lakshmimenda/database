package com.greatlearning.springbootrestaurantsapp.controller;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.springbootrestaurantsapp.entity.*;
import com.greatlearning.springbootrestaurantsapp.feignClients.OfflineUserClient;

@RestController
@RequestMapping("/Restaurants/OfflineUsers")
public class OfflineUserController {

	// offline user controller
	@Autowired
	private OfflineUserClient offlineUserClient;

	// gets all the items details based on id
	@GetMapping("/checkItem")
	public String checkItemExist(Long id) {
		return offlineUserClient.checkItemExist(id);

	}

	// gets all the items
	@GetMapping("/viewItems")
	public List<Items> viewItems() {

		return offlineUserClient.viewItems();
	}

	// allows the user to select items with multiple quantites
	@PostMapping("/selectItems")
	public String selectedItems(@RequestBody Map<Long, Integer> items) {
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		return offlineUserClient.selectedItems(items, userName);
	}

	// gets the total bill
	@GetMapping("/totalBill")
	public BigInteger totalBill() {
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		return offlineUserClient.totalBill(userName);
	}

	// allows the user to book event for 2 days later.
	@GetMapping("/bookEvent")
//	"Enter the date in mm/dd/yyyy format"
	public String bookEvent(String date) throws ParseException {
		return offlineUserClient.bookEvent(date);
	}

	// allows the user to enter the feedback in rating and text
	@GetMapping("/feedback")
	public String feedback(Integer rate, String feedback) {
		return offlineUserClient.feedback(rate, feedback);
	}

	// allows the user to make payment and apply coupon code if any
	@GetMapping("/payment")
	public String payment(String paymentMode, String code) {
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		return offlineUserClient.payment(paymentMode, code, userName);
	}

}
