package com.greatlearning.springbootrestaurantsapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.springbootrestaurantsapp.feignClients.FestiveSaleClient;

@RestController
@RequestMapping("/Restaurants/FestiveSales")
public class FestiveSaleController {

	// Festive Sales Controller
	@Autowired
	private FestiveSaleClient festiveSaleClient;
	
	//gets the coupon code
	
	@GetMapping("/couponCode")
	public String getCouponCode() {
		String userName = 	SecurityContextHolder.getContext().getAuthentication().getName();	
		return festiveSaleClient.getCouponCode(userName);
	}
	
}
