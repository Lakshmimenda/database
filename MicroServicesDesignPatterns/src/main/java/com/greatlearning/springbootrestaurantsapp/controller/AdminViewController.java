package com.greatlearning.springbootrestaurantsapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.springbootrestaurantsapp.feignClients.AdminViewClient;

@RestController
@RequestMapping("/Restaurants/AdminView")
public class AdminViewController {

	// Admin view controller to retrieve the sales details from Audit table for
	// different view
	@Autowired
	private AdminViewClient adminViewClient;

	// gets the sales for the per city
	@GetMapping("/getSalesPerCity")
	public String getSalesPerCity() {

		return adminViewClient.getSalesPerCity();
	}

	// gets the max sales for the month
	@GetMapping("/getMaxSalesOfTheMonth")
	public String getMaxSalesOfTheMonth() {

		return adminViewClient.getMaxSalesOfTheMonth();
	}

	// gets the sales per month for the last year
	@GetMapping("/getSalesPerMonthForLastYear")
	public String getSalesPerMonthForLastYear() {

		return adminViewClient.getSalesPerMonthForLastYear();
	}

}
