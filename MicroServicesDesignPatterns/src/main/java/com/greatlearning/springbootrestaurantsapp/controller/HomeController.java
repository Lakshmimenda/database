package com.greatlearning.springbootrestaurantsapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.springbootrestaurantsapp.entity.User;
import com.greatlearning.springbootrestaurantsapp.feignClients.AdminClient;

@RestController
public class HomeController {
	
//	Application Start Controller 
	
//	Auto wiring the User Services
	@Autowired
	private AdminClient adminClient;

	
//	End point to display the welcome message
	 @RequestMapping(value = "/", method = RequestMethod.GET)  
	    public String index() {  
	        return "Welcome!!! please redirect to http://localhost:8080/swagger-ui/";  
	    }  
	 
//	 End point for logging out 
	 @RequestMapping(value="/logout", method=RequestMethod.GET)  
	    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {  
	        Authentication auth = SecurityContextHolder.getContext().getAuthentication();  
	        if (auth != null){      
	           new SecurityContextLogoutHandler().logout(request, response, auth);  
	        }  
	         return "redirect:/";  
	     }  
	 
	 
//	 End point for registering the user. Exposed to all users irrespective of the right
	 @PostMapping("/registerUser")
		public String registerUser(@Validated @RequestBody User user)
		{
			return adminClient.registerUser(user);
		}
}
