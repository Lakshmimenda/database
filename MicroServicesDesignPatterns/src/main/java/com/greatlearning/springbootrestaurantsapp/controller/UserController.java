package com.greatlearning.springbootrestaurantsapp.controller;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.springbootrestaurantsapp.entity.*;
import com.greatlearning.springbootrestaurantsapp.feignClients.UserClient;

@RestController
@RequestMapping("/Restaurants/Items")
public class UserController {

	// user controller
	@Autowired
	private UserClient userClient;

	// gets the item based on id
	@GetMapping("/checkItem")
	public String checkItemExist(Long id) {
		return userClient.checkItemExist(id);
	}

	// gets all the items details
	@GetMapping("/viewItems")
	public List<Items> viewItems() {

		return userClient.viewItems();
	}

	// allows the user to select multiple items based on ids
	@PostMapping("/selectItems")
	public List<Items> selectedItems(@RequestBody Collection<Long> ids) {
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		return userClient.selectedItems(ids, userName);
	}

	// gets the total bill
	@GetMapping("/totalBill")
	public String totalBill(String code) {
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		return userClient.totalBill(userName, code);
	}

}
