package com.greatlearning.springbootrestaurantsapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class SpringbootRestaurantsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootRestaurantsApplication.class, args);
	}

}
