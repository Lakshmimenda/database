package com.greatlearning.springbootrestaurantsapp.feignClients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(url = "http://localhost:8084", name = "Festive-Sale-Client")
public interface FestiveSaleClient {

	// Feign Client mapping FestiveSales Services
	// gets the coupon code
	@GetMapping("/Restaurants/FestiveSales/couponCode")
	public String getCouponCode(@RequestParam("userName") String userName);
}
