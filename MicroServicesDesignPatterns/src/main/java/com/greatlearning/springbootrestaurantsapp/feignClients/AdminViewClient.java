package com.greatlearning.springbootrestaurantsapp.feignClients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(url = "http://localhost:8081", name = "Admin-View-Client")
public interface AdminViewClient {

	// Admin view controller to retrieve the sales details from Audit table for
	// different view

	// gets the sales for the per city
	@GetMapping("/Restaurants/AdminView/getSalesPerCity")
	public String getSalesPerCity();

	// gets the max sales for the month
	@GetMapping("/Restaurants/AdminView/getMaxSalesOfTheMonth")
	public String getMaxSalesOfTheMonth();

	// gets the sales per month for the last year
	@GetMapping("/Restaurants/AdminView/getSalesPerMonthForLastYear")
	public String getSalesPerMonthForLastYear();
}
