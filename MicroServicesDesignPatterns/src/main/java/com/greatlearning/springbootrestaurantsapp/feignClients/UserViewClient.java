package com.greatlearning.springbootrestaurantsapp.feignClients;

import java.text.ParseException;
import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.greatlearning.springbootrestaurantsapp.entity.Audit;

@FeignClient(url = "http://localhost:8082", name = "User-View-Client")
public interface UserViewClient {

	// gets the all the orders of the user
	@GetMapping("/Restaurants/ItemsView/allOrders")
	public List<Audit> allOrders(@RequestParam("userName") String userName);

	// get orders by date
	@GetMapping("/Restaurants/ItemsView/getOrdersByDate")
	public List<Audit> getOrdersByDate(@RequestParam("userName") String userName, @RequestParam("date") String date)
			throws ParseException;

	// get order by price
	@GetMapping("/Restaurants/ItemsView/getOrdersByPrice")
	public List<Audit> getOrdersByPrice(@RequestParam("userName") String userName, @RequestParam("price") int price);

}
