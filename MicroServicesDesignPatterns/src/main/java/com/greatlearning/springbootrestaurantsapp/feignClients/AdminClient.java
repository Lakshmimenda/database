package com.greatlearning.springbootrestaurantsapp.feignClients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import com.greatlearning.springbootrestaurantsapp.entity.*;

@FeignClient(url = "http://localhost:8081", name = "Admin-Client")
public interface AdminClient {
// Feign Client Mapping all the Admin Services

	// gets all the user details
	@GetMapping("/Restaurants/Admin/getAllUsers")
	public List<User> getAllUsers();

	// gets the user by id
	@GetMapping("/Restaurants/Admin/getUserById")
	public String getUserById(@RequestParam("id") Long id);

	// registers a user
	@PostMapping("/Restaurants/Admin/registerUser")
	public String registerUser(@Validated @RequestBody User user);

	// updates a user
	@PutMapping("/Restaurants/Admin/updateUser")
	public String updateUser(@RequestParam("id") Long id, @RequestBody User user);

	// deletes the user
	@DeleteMapping("/Restaurants/Admin/deleteUser")
	public String deleteUser(@RequestParam("id") Long id);

	// gets todays sales
	@GetMapping("/Restaurants/Audit/getBillsForToday")
	public List<Audit> getBillsForToday();

	// gets sales for the month
	@GetMapping("/Restaurants/Audit/getSalesForTheMonth")
	public List<Audit> getSalesForTheMonth();
}
