package com.greatlearning.springbootrestaurantsapp.feignClients;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.greatlearning.springbootrestaurantsapp.entity.*;

@FeignClient(url = "http://localhost:8083", name = "Offline-User-Client")
public interface OfflineUserClient {

	// Feign Client mapping all the offline User Services

	// gets the item details by id
	@GetMapping("/Restaurants/OfflineUsers/checkItem")
	public String checkItemExist(@RequestParam("id") Long id);

	// gets all the items details
	@GetMapping("/Restaurants/OfflineUsers/viewItems")
	public List<Items> viewItems();

	// selected Items for the users
	@PostMapping("/Restaurants/OfflineUsers/selectItems")
	public String selectedItems(@RequestBody Map<Long, Integer> items, @RequestParam("userName") String userName);

	// gets the total bill
	@GetMapping("/Restaurants/OfflineUsers/totalBill")
	public BigInteger totalBill(@RequestParam("userName") String userName);

	// allows user to book an event after 2 days
	@GetMapping("/Restaurants/OfflineUsers/bookEvent")
	public String bookEvent(@RequestParam("date") String date);

	// gets user feedback
	@GetMapping("/Restaurants/OfflineUsers/feedback")
	public String feedback(@RequestParam("rate") Integer rate, @RequestParam("feedback") String feedback);

	// allows user to make payment
	@GetMapping("/Restaurants/OfflineUsers/payment")
	public String payment(@RequestParam("paymentMode") String paymentMode, @RequestParam("code") String code,
			@RequestParam("userName") String userName);

}
