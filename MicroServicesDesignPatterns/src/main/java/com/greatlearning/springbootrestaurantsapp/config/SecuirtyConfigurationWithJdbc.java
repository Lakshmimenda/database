package com.greatlearning.springbootrestaurantsapp.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@Configuration
public class SecuirtyConfigurationWithJdbc extends WebSecurityConfigurerAdapter {

	// Enabling Spring Security

	@Autowired
	private DataSource dataSource;

// Authorizing users using jdbcAuthentication
	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().passwordEncoder(getPasswordEncoder()).dataSource(dataSource)
				.usersByUsernameQuery("select user_name, password, enabled from user where user_name=?")
				.authoritiesByUsernameQuery("select user_name, role from user where user_name=?");
	}

//	    Controlling the User Authorization per screen

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.csrf().disable().authorizeRequests().antMatchers("/registerUser").permitAll().antMatchers("/actuator/**").permitAll()
				.antMatchers("/Restaurants/Admin/**").hasAuthority("OWNER").antMatchers("/Restaurants/Items/**")
				.hasAuthority("CUSTOMER").antMatchers("/Restaurants/AdminView/**").hasAuthority("OWNER").antMatchers("/Restaurants/UserView/**")
				.hasAuthority("CUSTOMER").antMatchers("/Restaurants/OfflineUsers/**").hasAuthority("OFFLINEUSER")
				.antMatchers("/Restaurants/FestiveSales/**").permitAll().anyRequest().authenticated().and()

				.formLogin().permitAll().and().logout().permitAll();

	}

	// password Encoder
	@Bean
	PasswordEncoder getPasswordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}

// disabling security for H2 Console
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/h2-console/**");

	}
}
