package com.greatlearning.offlineServices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class OfflineUserMicroService {

	public static void main(String[] args) {
		SpringApplication.run(OfflineUserMicroService.class, args);
	}

}
