package com.greatlearning.offlineServices.controller;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.offlineServices.entity.Items;
import com.greatlearning.offlineServices.serviceImpl.OfflineUserServiceImpl;

@RestController
@RequestMapping("/Restaurants/OfflineUsers")
public class OfflineUserController {

	// Offline User Service Controller
	@Autowired
	private OfflineUserServiceImpl offlineUserService;

	// gets the items by id
	@GetMapping("/checkItem")
	public String checkItemExist(Long id) {
		return offlineUserService.checkItemExists(id);

	}

	// gets all the items
	@GetMapping("/viewItems")
	public List<Items> viewItems() {

		return offlineUserService.viewItems();
	}

	// allows the user to select multiple items with quantity
	@PostMapping("/selectItems")
	public String selectedItems(@RequestBody Map<Long, Integer> items, String userName) {
		return offlineUserService.selectedItems(items, userName).toString();
	}

	// gets the total bill
	@GetMapping("/totalBill")
	public BigInteger totalBill(String userName) {
		return offlineUserService.totalBill(userName);
	}

	// booking for event after 2 days
	@GetMapping("/bookEvent")
	public String bookEvent(String date) throws ParseException {
		return offlineUserService.bookEvent(date);
	}

	// allows the user to give feedback
	@GetMapping("/feedback")
	public String feedback(Integer rate, String feedback) {
		return offlineUserService.feedback();
	}

	// allows the user to perform the payments
	@GetMapping("/payment")
	public String payment(String paymentMode, String code, String userName) {
		return offlineUserService.makePayment(paymentMode, code, userName);
	}

}
